/**
Copyright (c) 2011-2014 Parjanya Mudunuri

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

http://opensource.org/licenses/mit-license.php
**/

apply plugin: 'java'
apply plugin: 'maven'
apply plugin: 'signing'
apply plugin: 'eclipse'
apply plugin: 'idea'
apply plugin: 'groovy'


//** CONFIGURATION **//
task wrapper(type: Wrapper) {
    gradleVersion = '2.0'
}

//** MAVEN CONFIGURATION **//
repositories {
    mavenCentral()
}

group='uk.co.firstzero'
version='2.8'
ext.packaging = 'jar'

/* To signal type of build which in turn sets the repository URL
 * gradle -Pdev 	//DEV BUILD
 * gradle -Pci  	//SNAPSHOT BUILD
 * gradle -Prelease //RELEASE BUILD
 */
def isDevBuild
def isCiBuild
def isReleaseBuild

def sonatypeRepositoryUrl

if(hasProperty("release")) {
	println "RELEASE build"
    isReleaseBuild = true
    sonatypeRepositoryUrl = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
} else if (hasProperty("ci")) {
	println "CI build"
    isCiBuild = true
    version += "-SNAPSHOT"
    sonatypeRepositoryUrl = "https://oss.sonatype.org/content/repositories/snapshots/"
} else {
	println "DEV build"
    isDevBuild = true
}

repositories {
    mavenCentral()
}

//** DEPENDENCIES **//
List compileAll = [ "org.apache.ant:ant:1.8.1", "junit:junit:4.5", "log4j:log4j:1.2.17", //All
				    "xmlunit:xmlunit:1.3", "net.sourceforge.jexcelapi:jxl:2.6.12", //uk.co.firstzero.xml.AntXMLUnit
				    "net.sf.opencsv:opencsv:2.0", //uk.co.firstzero.csv.diff
				    "net.sourceforge.jexcelapi:jxl:2.6.10", //uk.co.firstzero.csv.AntCsvToExcel
                    "commons-codec:commons-codec:1.6",
                    "commons-httpclient:commons-httpclient:3.0",
                    "commons-logging:commons-logging:1.1.1",
                    "org.apache.jackrabbit:jackrabbit-webdav:2.1.1",
                    "org.slf4j:slf4j-api:1.5.6",
                    "com.h2database:h2:1.3.170"       //uk.co.firstzero.sql - required for junit
                    ]

dependencies {
    compile compileAll
    compile gradleApi()		//For gradle custom tasks
    compile localGroovy()   //For gradle custom tasks
    runtime fileTree(dir: 'build/libs', include: '*.jar')	//For running the custom tasks locally
}

configurations {
    runtime {
        //extendsFrom compile
        fileTree(dir: 'build/libs', include: '*.jar')
    }
}

//** COMPILATION OPTIONS **//
tasks.withType(JavaCompile) {
    options.encoding = 'UTF-8'
    options.compilerArgs << "-Xlint:unchecked"
}

//** ARTIFACTS TO BE INCLUDED - REQUIRED FOR OSS MAVEN REPOSITORY **//
task javadocJar(type: Jar, dependsOn: javadoc) {
	classifier = 'javadoc'
	from 'build/docs/javadoc'
}

task sourcesJar(type: Jar) {
	from sourceSets.main.allSource
	classifier = 'sources'
}

artifacts {
	archives jar
	archives javadocJar
	archives sourcesJar
}

//** ARTIFACT SIGNING - REQUIRED FOR OSS MAVEN REPOSITORY **//
if(isReleaseBuild) {
    signing {
        sign configurations.archives
    }
} else {
    task signArchives {
        //Do nothing
    }
}

//** ARTIFACT UPLOADING - REQUIRED FOR OSS MAVEN REPOSITORY **//
uploadArchives {
	repositories {
		if (isDevBuild) {
            mavenDeployer {
                repository(url: uri('repo'))
            }
        }
		else {    
			mavenDeployer {
				if(isReleaseBuild) {
					beforeDeployment { MavenDeployment deployment -> signing.signPom(deployment) }
				}
				
				repository(url: sonatypeRepositoryUrl) {
				  authentication(userName: sonatypeUsername, password: sonatypePassword)
				}
	
				pom.project {
				   name 'AddOnJavaAntTasks'
				   packaging 'jar'
				   description 'Addon Ant Tasks'
				   url 'https://github.com/parj/AddOnJavaAntTasks'
	
				   scm {
					   url 'git@github.com:parj/AddOnJavaAntTasks'
					   connection 'scm:git:git@github.com:parj/AddOnJavaAntTasks.git'
					   developerConnection 'scm:git:git@github.com:parj/AddOnJavaAntTasks.git'
				   }
	
				   licenses {
					   license {
						   name 'MIT Licence'
						   url 'http://opensource.org/licenses/mit-license.php'
						   distribution 'repo'
					   }
				   }
	
				   developers {
					   developer {
						   id 'parj'
						   name 'Parj M'
					   }
				   }
				}	   
		   }
		}
	}
}


//** ANT TASKS, Gradle examples are below **//
/**
 * SAMPLE Ant Tasks - Uncomment to run 
 * pushAnt - Push file to a WebDav Server
 * pullAnt - Pull file from a webdav server
 * csvToExcelAnt - Converts csv to Excel
 * diffxmlAnt - Diffs two directories containing xml files with the same name
 * modifyPathAnt - Strips out and modifies tags in xmls and renames them using a naming convention
 * readBlobAnt - Reads blob data from the database
 *

// Push file to a WebDav Server
task pushAnt << {
    ant.taskdef(name: 'push', classname: 'uk.co.firstzero.webdav.Push', classpath: configurations.runtime.asPath)
    ant.push(user: 'admin', password: "admin", url: "http://localhost:8080/repository/default", overwrite: true) {
        fileset(dir: 'src/test/resources/webdav', includes: '*.csv')
    }
}

// Pull file from a webdav server
task pullAnt << {
    ant.taskdef(name: 'pull', classname: 'uk.co.firstzero.webdav.Pull', classpath: configurations.runtime.asPath)
    ant.pull(user: 'admin', password: "admin", url: "http://localhost:8080/repository/default",
            file: "output.csv",
            outFile: "src/test/resources/webdav/output.csv",
            overwrite: true)
}

// Converts csv to Excel
task csvToExcelAnt << {
    ant.taskdef(name: 'csvToExcel', classname: 'uk.co.firstzero.csv.AntCsvToExcel', classpath: configurations.runtime.asPath)

    ant.csvToExcel(outputFile: "build/tmp/report.xls", separator: "^") {
        fileset(dir: "src/test/resources/csv/CsvToExcel", includes: "*.csv")
    }
}

// Diffs two directories containing xml files with the same name
task diffxmlAnt << {
    ant.taskdef(name: 'diffxml', classname: 'uk.co.firstzero.xml.AntXMLUnit', classpath: configurations.runtime.asPath)

    ant.diffxml(testDirectory: "src/test/resources/xml/AntXMLUnitTest/test", resultDirectory: "out") {
        fileset(dir: "src/test/resources/xml/AntXMLUnitTest/control", includes: "*.xml")
    }
}

// Strips out and modifies tags in xmls and renames them using a naming convention
task modifyPathAnt << {
    ant.taskdef(name: 'xPath', classname: 'uk.co.firstzero.xml.AntXPath', classpath: configurations.runtime.asPath)
    ant.taskdef(name: 'modifyPath', classname: 'uk.co.firstzero.xml.ModifyPath', classpath: configurations.runtime.asPath)

    //Rename Pattern is the file rename pattern, takes as input as xpaths separated by #
    ant.xPath(outputDirectory: "out", renamePattern: "//publish_date[position() = 1]#_#//price[position() = 1]" ) {
        fileset(dir: "src/test/resources/xml/AntXPathTest", includes: "*.xml")
        modifyPath(path: "//title", delete:"True")
        modifyPath(path: "//author", value:"ToDo")
    }
}

// Reads blob data from the database
task readBlobAnt << {
    ant.taskdef(name: "readBlob", classname: "uk.co.firstzero.sql.AntReadBlob", classpath: configurations.runtime.asPath)

    String databaseLocation = projectDir.toString() + "/src/test/resources/sql/test"
    println ("Database = " + databaseLocation)

    //SQL should contain a string name and then blob - example
    //SELECT name, blob_data from test_db where condition_1=1234
    ant.readBlob(className: "org.h2.Driver", jdbcUrl: "jdbc:h2:" + databaseLocation + ";IFEXISTS=TRUE",
                 user: "sa", password: "", extension: ".jpg",
                 sql: "SELECT name, blob from TEST",
                 outputDirectory: "build/tmp",
                 unzip: "True")
}
**/

//** GRADLE Plugins - for the following to work run 'gradle uploadArchives' and then uncomment to run **//

/**
 * Uncomment all below after running gradle uploadArchives
 *
buildscript {
    repositories {
        maven {
            url uri('repo')
        }
        mavenCentral()
    }
    dependencies {
        classpath group: 'uk.co.firstzero', name: 'AddOnJavaAntTasks', version: '2.8'
    }
}

apply plugin: 'addonjavaanttasks'

// gradle csvToExcel Task
csvToExcelArgs {
	inputFiles = fileTree(dir: 'src/test/resources/csv/CsvToExcel', include: 'output.csv')
	outputFile = 'src/test/resources/csv/CsvToExcel/report.xls'
	separator = ','
}

// gradle csvDiffTask
csvDiffArgs {
	resultDirectory = projectDir.toString() + "/src/test/resources/csv/CsvDiff";
	separator = ","
	controlDirectory = fileTree(dir: 'src/test/resources/csv/CsvDiff/control', include: '*.csv')
	testDirectory = 'src/test/resources/csv/CsvDiff/test'
	keyColumns = 'Header_1;Header_3'
}

// gradle xmlCleanTask
xmlClean {
	inputDirectory = fileTree(dir: 'src/test/resources/xml/AntXPathTest', include: '*.xml')
	outputDirectory = 'src/test/resources/xml/AntXPathTest'
	renamePattern = '//publish_date[position() = 1]#_#//price[position() = 1]'
	modifyPaths = [ new uk.co.firstzero.xml.ModifyPath(path: "//title", delete:"True"), 
					new uk.co.firstzero.xml.ModifyPath(path: "//author", value:"ToDo")]
}

// gradle xmlDiffTask
xmlDiffArgs {
	resultDirectory = projectDir.toString() + "/src/test/resources/xml/AntXMLUnitTest";
	separator = ","
	controlDirectory = fileTree(dir: 'src/test/resources/xml/AntXMLUnitTest/control', include: '*.xml')
	testDirectory = 'src/test/resources/xml/AntXMLUnitTest/test'
}

// gradle readBlobTask
readBlobArgs {
    className = "org.h2.Driver"
    String databaseLocation = projectDir.toString() + "/src/test/resources/sql/test"
    jdbcUrl = "jdbc:h2:" + databaseLocation + ";IFEXISTS=TRUE"
    user = "sa"
    password = ""
    extension = ".jpg"
    sql = "SELECT name, blob from TEST"
    outputDirectory = "build/tmp"
    unzip = true
}

// gradle pullTask
pullArgs {
    user = 'admin'
    password = "admin"
    url = "http://localhost:8080/repository/default"
    file = "output.csv"
    outFile = "src/test/resources/webdav/output.csv"
    overwrite = true
}

// gradle pushTask
pushArgs {
    user = 'admin'
    password = "admin"
    url = "http://localhost:8080/repository/default"
    overwrite = true
    tree = fileTree(dir: 'src/test/resources/webdav', include: '*.csv')
    createDirectoryStructure = false
}
**/